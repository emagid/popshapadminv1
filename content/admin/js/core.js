var config = {
    apiKey: "AIzaSyAYYtHwNNZmXlUnogyHuT0kYZ8-Cp1eSI8",
    authDomain: "popshap-admin-dev.firebaseapp.com",
    databaseURL: "https://popshap-admin-dev.firebaseio.com",
    projectId: "popshap-admin-dev",
    storageBucket: "popshap-admin-dev.appspot.com",
    messagingSenderId: "638958152185"
  };
var curr_user;
var db;
$(document).ready(function(){
    firebase.initializeApp(config);
    db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            db.collection('users').doc(user.uid).get().then(function(_user){
                curr_user = _user.data();
                curr_user.displayName = firebase.auth().currentUser.displayName; 
                curr_user.phoneNumber = firebase.auth().currentUser.phoneNumber; 
                init();
            });
        } else {
          if(window.location.pathname != "/admin/login"){     
            window.location.href = "/admin/login/logout";
          } else {
            init();
          }
        }
    });
 });