<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Popshap | <?=$this->emagid->route['controller']?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL . 'content/admin/material/css/material-dashboard.min.css?v=2.1.1'?>" />
    <link type="text/css" rel="stylesheet" href="<?=SITE_URL . 'content/admin/css/admin_new.css'?>" />

    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-database.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-functions.js"></script>
    <script src="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.js"></script>
    <link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.css" />
    
    <script src=""></script>
  </head>
  <body>

    <div class="wrapper ">
    <sidebar class="sidebar" data-color="purple" data-background-color="white">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag -->
      <div class="logo">
        <a class="simple-text logo-mini">
          Popshap
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <? require("sidebar.php");?>
          <!-- your sidebar here -->
        </ul>
      </div>
</sidebar>
    <div class="main-panel">
      <!-- Navbar -->
        <? require("header.php");?>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          
          <?php $emagid->controller->renderBody($model); ?>
        </div>
      </div>
      <footer class="footer">
        <? require("footer.php"); ?>
      </footer>
    </div>
  </div>

  <!--   Core JS Files   -->
  <script src="<?=SITE_URL . 'content/admin/material/js/core/jquery.min.js'?>" type="text/javascript"></script>
  <script src="<?=SITE_URL . 'content/admin/material/js/core/popper.min.js'?>" type="text/javascript"></script>
  <script src="<?=SITE_URL . 'content/admin/material/js/core/bootstrap-material-design.min.js'?>" type="text/javascript"></script>
  <script src="<?=SITE_URL . 'content/admin/material/js/plugins/perfect-scrollbar.jquery.min.js'?>"></script>
  <? if(true) { ?>
  <!--  Google Maps Plugin    -->
  <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVAHs4wZEk0HvBr8XaUK8D5PgN4wKFRkQ"></script> -->
  <!-- Chartist JS -->
  <script src="<?=SITE_URL . 'content/admin/material/js/plugins/chartist.min.js'?>"></script>
  <!--  Notifications Plugin    -->
  <script src="<?=SITE_URL . 'content/admin/material/js/plugins/bootstrap-notify.js'?>"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?=SITE_URL . 'content/admin/material/js/material-dashboard.js?v=2.1.1'?>" type="text/javascript"></script>
  <? } ?>
  <?php script('core.js',ADMIN_JS); ?>
  <script type='text/javascript'>
    function slug_async(in_elem,out_elem) {
      in_elem.on('keyup',function(e) {
        var val = $(this).val();
        val = val.replace(/[^\w-]/g, '-');
        val = val.replace(/[-]+/g,'-');
        out_elem.val(val.toLowerCase());
      });
    }
    var params = <?php echo (isset($model->params)) ? json_encode($model->params) : json_encode((object)array()); ?>;
      // in case result is an array, change it to object
    if(params instanceof Array) {
      params = {};
    }
    $(document).ready(function() {
      /**
       * builds a url with params from a params object passed to it
       * @param {type} url: url of page
       * @param {type} params: params object with key as param key name and value as param value
       * @param {type} redirect: true or false if we want to redirect to the url
       * @returns {Boolean|String}
       */
      function build_url(url,params,redirect) {
      
        var params_arr = [];
      $.each(params,function(i,e) {
          params_arr.push(i+"="+e);
        });
        if(redirect) {
          window.location.href = url + "?"+params_arr.join("&");
          return false;
        } else {
          return url + "?"+params_arr.join("&");
        }
      }

      if (typeof total_pages !== 'undefined' && typeof page !=='undefined') {
      // the variable is defined
        $(function() {
            $('div.paginationContent').pagination({
                pages: total_pages,
                currentPage: page,
                cssStyle: 'light-theme',
                onPageClick: function(pageNumber,event) {
                  var url_params = params || {};
                  url_params.page = parseInt(pageNumber);
                  var full_url = site_url;
                  build_url(full_url,url_params,true);
                //window.location.href = full_url+"?page="+page;
                }
            });
        });
      }
    });
  </script>
  </body>
</html>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.full.js"></script>

<? 
function footer()  {
   script('jquery.min.js',ADMIN_JS); 
} ?>