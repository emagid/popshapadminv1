<?php 
/** Setting up an EmagidPHP project . **/ 

define('SITE_NAME' , 'Allied' );
define('SITE_EMAIL' , 'allied@emagid.com' );


define("ROOT_DIR",__DIR__.'/../');
define('DS' , DIRECTORY_SEPARATOR);
define('SITE_DOMAIN' , $_SERVER['SERVER_NAME'] );

// $site_url = dirname($_SERVER['SCRIPT_NAME'])."/";
// if($site_url=='//') {$site_url='/';} 
$site_url = '/';
define('SITE_URL' , $site_url );

// FRONTEND
define('FRONT_IMG' , SITE_URL . 'content/frontend/images/' );
define('FRONT_JS' , SITE_URL . 'content/frontend/js/' );
define('FRONT_CSS' , SITE_URL . 'content/frontend/css/' );
define('FRONT_ASSETS' , SITE_URL . 'content/frontend/assets/' );
define('FRONT_LIBS' , SITE_URL . 'content/frontend/libs/' );

// ADMIN
define('ADMIN_URL' , SITE_URL . 'admin/' );
define('ADMIN_IMG' , SITE_URL . 'content/admin/images/' );
define('ADMIN_JS' , SITE_URL . 'content/admin/js/' );
define('ADMIN_CSS' , SITE_URL . 'content/admin/css/' );
define('ADMIN_ICONS' , SITE_URL . 'content/admin/icons/' );

// UPLOAD
define('IMAGE_UPLOAD' , SITE_URL . '/content/uploads/' );
define('UPLOAD_URL' , SITE_URL . 'content/uploads/');
define('UPLOAD_PATH' , ROOT_DIR . DS. 'content'.DS.'uploads'.DS);

define('MIN_URL' , SITE_URL . 'content/media/min/');
define('MIN_PATH' , ROOT_DIR . DS.'content'.DS.'media'.DS.'min'.DS);

define('THUMB_URL' , SITE_URL . 'content/media/thumb/');
define('THUMB_PATH' , ROOT_DIR . DS.'content'.DS.'media'.DS.'thumb'.DS);

//Firebase
define('FIREBASE_CREDENTIALS',__DIR__.'/firebase_credentials.json');

define('CRYPT_KEY' , 'emagidphp');//password to (en/de)crypt

// image sizes
$image_sizes = [[100,100],[400,300],[1024,768]];