<?php

namespace Emagid;


if ($_SERVER['DOCUMENT_ROOT'])
{
  $folder = dirname(dirname(__DIR__));  // assuming that the library is in '/lib/emagid/'


  set_include_path($folder);

  define('ROOT_PATH',
    $_SERVER['DOCUMENT_ROOT'] . (defined('ROOT_ADD') ? DIRECTORY_SEPARATOR . ROOT_ADD : ''));
}



require_once("includes" . DIRECTORY_SEPARATOR . "functions.inc.php");

require_once("hooks/__autoloader.php");

spl_autoload_register(['\Emagid\AutoLoader', 'loadNamespace']);



/**
 * Base class for eMagid libraries 
 */
class Emagid
{

  /**
   * @var Object 
   * 	page object, will contain SEO , data and other page specific handlers
   */
  public $page = null;

  /**
   * @var Determine whether to include debug module or not .
   */
  public $debug = false;

  /**
   * @var String base path
   */
  public $base_path = '/';


  /**
   * @var String base path
   */
  public $root = '/';




  /**
   * @var Array list of folders to include 
   */
  public $include_paths = [];

  /**
   * @var an object that contains the connection string parameters (db_name, username, password, host)
   */
  public $connection_string;

  /**
   * @var Object \Emagid\Mvc\Conroller.
   */
  public $controller;

  /**
   * @var string acive template's name
   */
  public $template = null;

  /**
   * @var string 
   */
  public $template_path = null;

  /**
   * @var int default error_reporting value 
   */
  private static $default_error_reporting = 0;
  
  public $db;

  /**
   * Defualt constructor 
   *
   * $params Array basic settings, such as directories, DB connection etc... 
   */
  public function __construct($params = [])
  {

    $this->connection_string = new \stdClass;



    if (count($params) > 0) {
      $this->readParams($params);
    }



    if ($this->debug)
    { // load kint for debugging 
      require_once(__DIR__ . DIRECTORY_SEPARATOR . '_includes' . DIRECTORY_SEPARATOR . 'kint' . DIRECTORY_SEPARATOR . 'Kint.class.php');
    }

    //$this->page = new \Emagid\Page\Page();


    if ($this->debug)
    {
      //set_error_handler('\Emagid\Emagid::emagidErrorHandler');
    }

    $this->loadDefaults(); 

    foreach ($_GET as $key => $value)
    {
      if (startsWith($key, 'emagid_'))
      {
        $func = substr($key, strlen('emagid_'));

        $this->{$func}();
      }
    }

    if ($this->template) {
      $this->template_path =  str_replace('//', '/',  $this->root.'/templates/'.$this->template.'/' );
    }

    return $this;
  }



  /**
  * Load default variables to be used on the page
  */ 
  private function loadDefaults () {
    $this->uri = $_SERVER['REQUEST_URI']; 
  }

  /**
   * Breaks the parameters from the constructor into local variables.
   */
  private function readParams($params)
  {
    foreach ($params as $key => $value)
    {
      if (is_array($value))
      {
        $this->{$key} = $this->array_to_object($value);
      }
      else
      {
        $this->{$key} = $value;
      }
    }
  }

  /**
   * convert an array to an object 
   *
   * @param Array
   * @return strClass 
   */
  private function array_to_object($array)
  {
    $obj = new \stdClass;

    foreach ($array as $k => $v)
    {
      if (is_array($v))
      {
        $obj->{$k} = $this->array_to_object($v); //RECURSION
      }
      else
      {
        $obj->{$k} = $v;
      }
    }

    return $obj;
  }


  /**
   * Load the emagid MVC framework 
   */
  public function loadMvc(Array $params = [])
  {

    if (!isset($params['root']))
      $params['root'] = $this->root;

      
  
    \Emagid\Mvc\Mvc::Load($params);

    return $this;
  }

  /**
   * Initializer the library's internal debugger and prevent PHP errors from showing 
   */
  static function startDebugger()
  {
    self::$default_error_reporting = ini_get('error_reporting');

    error_reporting(0);
  }

  static function emagidErrorHandler($errno, $errstr, $errfile, $errline)
  {


    \Kint::trace();
  }
  
  public function getDb()
  {
    if (!$this->db){
      $this->db = new \Emagid\Core\PdoConn();
    }

    return $this->db; 
  }

}

/**
 * Checks whether a strings starts with a specific string.
 *
 * @todo Move this function to functions.inc.php
 */
function startsWith($haystack, $needle, $case = true)
{
  if ($case)
    return strpos($haystack, $needle, 0) === 0;

  return stripos($haystack, $needle, 0) === 0;
}
