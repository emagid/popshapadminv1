<style>
  th.sortable {
    cursor: pointer;
    font-weight: bold !important;
  }
  th.sortable[data-direction=asc] {
    color: darkgreen;
  }
  th.sortable[data-direction=desc] {
    color: darkred;
  }
</style>
<div class="content">
  <div class="container-fluid">
    <div id="inv_counts" class="row">
      <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="card card-stats _office">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">home</i>
            </div>
            <p class="card-category inv_group_title">Showroom Count</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <!-- <i class="material-icons text-danger">warning</i>
              <a href="#pablo">Get More Space...</a> -->
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="card card-stats _warehouse">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">inventory</i>
            </div>
            <p class="card-category inv_group_title">Amount In Warehouse</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <!-- <i class="material-icons text-danger"></i>
              <a href="#pablo"></a> -->
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="card card-stats _client">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">content_copy</i>
            </div>
            <p class="card-category inv_group_title">Devices w/ Clients</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <!-- <i class="material-icons text-danger">warning</i>
              <a href="#pablo">Get More Space...</a> -->
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <div class="card card-stats _client">
          <div class="card-header card-header-warning card-header-icon">
            <div class="card-icon">
              <i class="material-icons">build</i>
            </div>
            <p class="card-category inv_group_title">Devices In Service</p>
          </div>
          <div class="card-footer">
            <div class="stats">
              <!-- <i class="material-icons text-danger">warning</i>
              <a href="#pablo">Get More Space...</a> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> View Users</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th class='sortable' data-direction="desc" data-field="sku" width='40%'> SKU </th>
                  <th class='sortable' data-field="serial_num" width='3%'> Serial # </th>
                  <th class='sortable' data-field="man_name" width='5%'> Man. Name </th>
                  <th class='sortable' data-field="device_type" width='10%'> Device Type </th>
                  <th class='sortable' data-field="size" width='3%'> Size </th>
                  <th class='sortable' data-field="color" width='4%'> Color </th>
                  <th class='sortable' data-field="location" width='5%'> Status/Location </th>
                  <th class='sortable' data-field="client" width='10%'> Client </th>
                  <th width='20%' colspan='2'> Controls </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
    var device_types;
    var colors;
    var devices;
    var sorts = {sku: 'desc'};
    var doc;
    function getSortedDoc(){
      doc = db.collection('devices');
      for(key in sorts){
        if(sorts[key] != false){
          doc = doc.orderBy(key,sorts[key]);
        }
      }
      return doc;
    }
    function loadDevices(){
      getSortedDoc()
      .get().then(function(querySnapshot){
        $('.items').empty();
        querySnapshot.forEach(function(doc) {
          var row = $('<tr class="device">');
          row.prop('id',doc.id);
          row.data('id',doc.id);
          // row.append('<td>'+doc.id+'</td>');
          data = doc.data();

          row.append('<td><input class="form-control" type="text" placeholder="Internal Sku" name="sku" value="'+(data.sku?data.sku:'')+'" /> </td>');
          row.append('<td><input class="form-control" type="text" placeholder="Manufacturer Serial #" name="serial_num" value="'+(data.serial_num?data.serial_num:'')+'" /> </td>');
          row.append('<td><input class="form-control" type="text" placeholder="Manufacturer Name" name="man_name" value="'+(data.man_name?data.man_name:'')+'" /> </td>');
          

          var device_select = $('<td>'+data.device_type+'</td>');;
          row.append(device_select);
          createSelect(device_select,'device_types',false,'name',data.device_type,'device_type');
          row.data('device_type',data.device_type);

          row.append('<td><input class="form-control" type="number" placeholder="Size in inches" name="size" value="'+data.size+'" /> </td>');
          row.append('<td><input class="form-control" type="text"   placeholder="Color" name="color" value="'+data.color+'" /> </td>')
          
          var location_select = $('<td>'+(data.location?data.location:'')+'</td>');
          row.append(location_select);
          createSelect(location_select,'locations',false,'name',(data.location?data.location:false),'location');
          row.data('location',(data.location?data.location:''));
          
          var client_select = $('<td>'+(data.client?data.client:'')+'</td>');
          row.append(client_select); 
          createSelect(client_select,'clients',false,'name',(data.client?data.client:false),'client',true);
          row.data('client',(data.client?data.client:''));
          
          if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
            row.append('<td><a class="btn save"> save</a></td>');
            row.append('<td><a class="btn delete"> delete</a></td>');
          } else {
            row.append('<td></td>');
            row.append('<td></td>');
          }
          $('.items').append(row);
        });
        var new_row = $('<tr class="device">');
        var device_select = $('<td></td>');
        var location_select = $('<td></td>');
        var client_select = $('<td></td>');

        new_row.append('<td><input class="form-control" type="text" placeholder="Internal Sku" name="sku" /> </td>');
        new_row.append('<td><input class="form-control" type="text" placeholder="Manufacturer Serial #" name="serial_num" /> </td>');
        new_row.append('<td><input class="form-control" type="text" placeholder="Manufacturer Name" name="man_name" /> </td>');
        new_row.append(device_select);
        new_row.append('<td><input class="form-control" type="number" name="size"  placeholder="Size in inches" value="" /> </td>');
        new_row.append('<td><input class="form-control" type="text"   name="color" placeholder="Color" value="" /> </td>')
        $('.items').append();
        new_row.append(location_select);
        new_row.append(client_select);
        new_row.append('<td colspan="2"><a class="btn add"> Add</a></td>');

        createSelect(device_select,'device_types',false,'name',false,'device_type');
        createSelect(location_select,'locations',false,'name',false,'location');
        createSelect(client_select,'clients',false,'name',false,'client',true);

        if(curr_user.user_role == 'superadmin'){
          $('.items').append(new_row);
        }
      });
    }
    /*
    * @param {Object} element - The Jquery <td> element to be replaced
    * @param {string} collection - Name of the collection to pull options from 
    * @param {string} value_field - The field of the record to set the options values. defaults to the id
    * @param {string} display_field - The field of the record to display. defaults to the id
    * @param current_value - The currentvalue, if any of the field. if undefined no option will be preselected
    * @param {string} field_name - the name to set the select to, defaults to collection value 
    * @param {boolean} null_option - Whether or not to include an option to set to null
    */
    function createSelect(element, collection, value_field, display_field, current_value, field_name, null_option){
      field_name = field_name?field_name:collection;
      var select = $("<select class='form-control' name='"+field_name+"'>"+
                      "</select>"); 
                      select.append("<option disabled selected > Select a "+field_name.replace(/_/g,' ')+"</option>");
                      if(null_option){
                        select.append("<option value='' > N/A </option>");
                      }
      db.collection(collection)
      .get().then((querySnapshot) => {
        querySnapshot.forEach(function(doc) {
          var data = doc.data();
          var val = data.hasOwnProperty(value_field)?data[value_field]:doc.id;
          var display = data.hasOwnProperty(display_field)?data[display_field]:doc.id; 
          var option = $("<option value='"+val+"'>"+(display.replace(/_/g,' ')
              .split(' ')
              .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
              .join(' '))+"</option>");
              if(current_value === val){
                option.prop('selected',true);
              }
          select.append(option);
        });
        element.empty().append(select); 
      });

    }
    function init(){
      loadDevices();
      
      if(curr_user.user_role == 'superadmin'){
        $(document).on('click','a.btn.add',function(){
          var data = {};
          var row = $(this).parents('tr.device');
          var id = row.data('id');
          row.find(':input').serializeArray().forEach(function(elem) {
            if(elem.name != 'id'){
              data[elem.name] = (elem.value == ''?null:elem.value);
            } else {
              id = elem.value;
            }
          });
          if(id){
            db.collection('devices')
            .doc(id)
            .set(data,{merge: true})
            .then(function(){
              loadDevices();
            }); 
          } else {
          db.collection('devices')
           .add(data)
           .then(function(){
             loadDevices();
           }); 
          }
        });
        
        $('th.sortable').click(function(){
          if(!sorts.hasOwnProperty($(this).data('field'))){
            sorts = {};
              $('th.sortable').not(this)
              .data('direction',false)
              .attr('data-direction',false);
          }
          switch($(this).data('direction')){
            case 'asc':
              $(this).data('direction','desc')
              .attr('data-direction','desc');
              break;
            case 'desc':
              $(this).data('direction',false)
              .attr('data-direction',false);
              break;
            default:
              $(this).data('direction','asc')
              .attr('data-direction','asc'); 
              break;
          }
          sorts[$(this).data('field')] = $(this).data('direction');
          loadDevices();
        });
        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.device').data('id');
          var row = $(this).parents('tr.device');
          db.collection("devices").doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.device').data('id');
          var data = {};
          var row = $(this).parents('tr.device');
          row.find(':input').serializeArray().forEach(function(elem) {
            if(elem.name != 'id'){
              data[elem.name] = (elem.value == ''?null:elem.value);
            }
          });
          db.collection('devices').doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        $(document).on('change','select[name=location]',function(){
          var row = $(this).parents('tr.device');
          var client_cell = row.find('select[name=client]').parent('td');
          if(this.value == 'client'){
            client_cell.css('cursor','auto');
            client_cell.find('select').css('pointer-events','auto');
          } else {
            client_cell.css('cursor','not-allowed');
            client_cell.find('select').css('pointer-events','none').val('');
          }
        });
      }
    }
</script>