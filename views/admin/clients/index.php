<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> View Users</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th> ID </th>
                  <th> Client</th>
                  <th> Industry </th>
                  <th> Email</th>
                  <th> Phone </th>
                  <!-- <th> Users </th> -->
                  <th colspan='2'> edit </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    function loadClients(){
      db.collection('clients')
        .orderBy('name','asc')
        .get().then(function(querySnapshot) {
        $('.items').empty();
        querySnapshot.forEach(function(doc) {
          var row = $('<tr class="client">');
          row.data('id', doc.id);
          data = doc.data();
          row.append('<td>'+doc.id+'</td>');
          row.append('<td>'+(data.name?data.name:'')+'</td>');
          row.append('<td>'+(data.industry?data.industry:'')+'</td>');
          row.append('<td>'+(data.email?data.email:'')+'</td>');
          row.append('<td>'+(data.phone?data.phone:'')+'</td>');
          
          if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
            row.append('<td><a class="btn" href="/admin/clients/update/'+doc.id+'"> Edit</a></td>');
            row.append('<td><a class="btn delete"> delete</a></td>');
          } else {
            row.append('<td></td>');
            row.append('<td></td>');
          }
          $('.items').append(row);
          // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });

        var new_row = $('<tr class="client">');

        new_row.append('<td colspan="5"> New Client </td>');
        new_row.append('<td colspan="2"><a class="btn add" href="/admin/clients/update/"> Add</a></td>');

        if(curr_user.user_role == 'superadmin'){
          $('.items').append(new_row);
        }

      });
    } 
    function init(){
      
      loadClients();
      
      if(curr_user.user_role == 'superadmin'){
        $(document).on('click','a.btn.add',function(){
          // var data = {};
          // var row = $(this).parents('tr.client');
          // var id;
          // row.find(':input').serializeArray().forEach(function(elem) {
          //   if(elem.name != 'id'){
          //     data[elem.name] = (elem.value == ''?null:elem.value);
          //   } else {
          //     id = elem.value;
          //   }
          // });
          // db.collection('clients')
          //  .add(data)
          //  .then(function(){
          //    loadClients();
          //  }); 
        });

        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.client').data('id');
          var row = $(this).parents('tr.client');
          db.collection("clients").doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.client').data('id');
          var data = {};
          var row = $(this).parents('tr.client');
          row.find(':input').serializeArray().forEach(function(elem) {
            if(elem.name != 'id'){
              data[elem.name] = (elem.value == ''?null:elem.value);
              if(elem.name == 'contact_email' && elem.value.includes(',') ){
                data[elem.name] = elem.value.split(',');
              }
            }
          });
          db.collection('clients').doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        // $(document).on('change','textarea.notes',function(){
        //   var id = $(this).parents('tr.user').data('id');
        //   db.collection('users').doc(id).set({
				// 		notes: this.value
				// 	},{ merge: true }).then(function(){
				// 	});
        // });
      }
    }
</script>