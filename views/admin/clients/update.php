<div class="card card-nav-tabs">
    <div class="card-header card-header-primary">
        <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
        <div class="nav-tabs-navigation">
            <div class="nav-tabs-wrapper">
                <ul class="nav nav-tabs" data-tabs="tabs">
                    <li class="nav-item">
                        <a class="nav-link active" href="#general" data-toggle="tab">general</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#documents" data-toggle="tab">documents <span id="documents_count" class="em-notification">0</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contacts" data-toggle="tab">contacts <span id="contacts_count" class="em-notification">0</span></a>
                    </li>
                </ul>
            </div>
        </div>
	</div>
	<div class="card-body ">
        <div class="tab-content text-center">
            <div class="tab-pane active" id="general">
				<form id='client_form'>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Company Name</label>
								<input type="text" name="name" class="form-control" placeholder="Name of the Company/Client">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Industry</label>
								<input type="text" name="industry" class="form-control" placeholder="Industry of the Company/Client">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Email</label>
								<input type="email" name="email" class="form-control" placeholder="Email Address of the Company/Client">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="bmd-label-static">Phone</label>
								<input type="tel" name="phone" class="form-control" placeholder="Phone Number of the Company/Client">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<div class="form-group">
								<label class="bmd-label-static">Billing Address</label>
								<textarea type="text" name="billing_address" class="form-control" placeholder="Company Billing Address"></textarea>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<label style="left:auto; right:0" class="bmd-label-static"><input type="checkbox" id="same_billing" /> <span>Same As Billing</span></label>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								<label class="bmd-label-static">Shipping Address</label>
								<textarea type="text" name="shipping_address" class="form-control" placeholder="Company Shipping Address"></textarea>
							</div>
						</div>
					</div>
					<input type="submit" class="btn btn-success btn_save" value="Save" />
				</form>
			</div>
            <div class="tab-pane" id="documents">
			<button type="button" class="btn btn-success btn-round" id="new_doc_btn"><i class="material-icons">+</i></button>
				<div class="row" id="docs_row">
					<div class="col-md-6 doc_card hidden card" >
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Document Name</label>
										<input type="text" name="name" class="form-control" placeholder="Name of The Document">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Document Link</label>
										<input type="text" name="link" class="form-control" placeholder="Link to the document">
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="btn btn-primary btn_save">Save</button>
							<button type="button" class="btn btn-danger  btn_delete">Delete</button>
						</div>
					</div>
				</div>
			</div>
            <div class="tab-pane" id="contacts">
				<button type="button" class="btn btn-success btn-round" id="new_contact_btn"><i class="material-icons">+</i></button>
				<div class="row" id="contacts_row">
					<div class="col-md-6 contact_card hidden card" >
						<div class="card-header card-header-primary">
							<h4 class="card-title contact_name"><input type="text" name="name" placeholder="Contact Name" class="form-control" /> </h4>
							<p class="category contact_title"><input type="text" name="title" placeholder="Contact Title" class="form-control" /></p>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Email</label>
										<input type="email" name="email" class="form-control" placeholder="Email Address of the Company/Client">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label class="bmd-label-static">Phone</label>
										<input type="email" name="phone" class="form-control" placeholder="Phone Number of the Company/Client">
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="bmd-label-static">Notes</label>
										<textarea name="notes" class="form-control" placeholder="Notes"></textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="button" class="btn btn-primary btn_save">Save</button>
							<button type="button" class="btn btn-danger  btn_delete">Delete</button>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
  </div>
<?php footer();?>
<style>
	.hidden {
		display: none;
	}
</style>
<script type="text/javascript">
	var batch;
	var contacts_cnt = 0;
	var docs_cnt = 0;
	var client = {};
	var client_doc = {};
	function init(){
		batch = db.batch();
		client_doc = db.collection('clients').doc(<?=isset($model->record_id)?"'$model->record_id'":''?>);
		client_doc.get()
		.then(function(doc) {
			if(!doc.exists){
				client_doc = db.collection('clients').doc();
			}
		});
		$('#same_billing').change(function(){
			var bill_add = $('[name=billing_address]');
			var ship_add = $('[name=shipping_address]');
			if($(this).is(':checked')){
				ship_add.val(bill_add.val()).prop('readonly',true);
			} else {
				ship_add.val('').prop('readonly',false);
			}
		});
		$('#client_form').submit(function(e){
			e.preventDefault();
			var data = objectifyForm($('#client_form').serializeArray());
			batch
			.set(client_doc, data, {merge: true})
			.commit()
			.then(function(){
				console.log("Document successfully written!");
				batch = db.batch(); 
			}).catch(function(error){
				alert('Failed to save Data!');
			});
		});
		$(document).on('click','.contact_card .btn_save',function(){
			var card = $(this).parents('.contact_card');
			save_contact(card);
		});
		$(document).on('click','.doc_card .btn_save',function(){
			var card = $(this).parents('.doc_card');
			save_document(card);
		});
		load_client();
		$('#new_contact_btn').not('[disabled]').click(function(){
			new_contact();
		});
		$('#new_doc_btn').not('[disabled]').click(function(){
			new_document();
		});
	}
	function load_client(){
		client_doc 
		.get()
		.then(function(doc) {
			if(doc.exists) {
				client = doc.data();
			} else {
				client = {}
			}
			var form = $('#client_form');
			for(var key in client ){
				if(typeof client[key] !== 'object' ){
					var input = form.find('[name='+key+']');
					input.val(client[key]);
				}
			}
			build_contacts();
			build_docs();
		});

	}
	function build_docs(){
		docs_cnt = 0;
		$('#new_doc_btn').prop('disabled',false);
		$('#docs_row .doc_card').not('.hidden').remove();
		client_doc.collection('documents')
		.get().then((docs) => {
			docs.forEach(function(_doc){
				docs_cnt ++;
				$('#documents_count').text(docs_cnt);
				doc = _doc.data();
				var doc_card = $('.doc_card.hidden').clone();
				doc_card.removeClass('hidden');
				doc_card.data('id',_doc.id);
				for(i in doc){
					doc_card.find('[name='+i+']').val(doc[i]);
				}
				$('#docs_row').append(doc_card);
			});
		});
	}
	function build_contacts(){
		contacts_cnt = 0;
		$('#new_contact_btn').prop('disabled',false);
		$('#contacts_row .contact_card').not('.hidden').remove();
		client_doc.collection('contacts')
		.get().then((contacts) => {
			contacts.forEach(function(_contact){
				contacts_cnt ++;
				$('#contacts_count').text(contacts_cnt);
				contact = _contact.data();
				var contact_card = $('.contact_card.hidden').clone();
				contact_card.removeClass('hidden');
				contact_card.data('id',_contact.id);
				for(i in contact){
					contact_card.find('[name='+i+']').val(contact[i]);
				}
				$('#contacts_row').append(contact_card);
			});
		});
	}
	function new_contact(){
		// $('#new_contact_btn').prop('disabled',true);
		var empty_contact = $('.contact_card.hidden').clone();
		empty_contact.removeClass('hidden');
		$('#contacts_row').append(empty_contact);
	}
	function new_document(){
		// $('#new_contact_btn').prop('disabled',true);
		var empty_doc = $('.doc_card.hidden').clone();
		empty_doc.removeClass('hidden');
		$('#docs_row').append(empty_doc);
	}
	function save_document(doc_card){ //Accepts jQuery element of card
		var doc_data = objectifyForm(doc_card.find(':input').serializeArray());
		var doc_id = doc_card.data('id');
		var doc_doc;
		if(doc_id){
			doc_doc = client_doc.collection('documents').doc(doc_id);
		} else {
			doc_doc = client_doc.collection('documents').doc();
		}
		client_doc.get().then(function(doc){
			if(doc.exists){
				doc_doc.set(doc_data,{merge:true}).then(function(){
					console.log('success');
					build_docs();
				});
			} else {
				batch.set(doc_doc, doc_data, {merge: true});
				alert('You must finish saving this new client to save your document changes');
			}
		});
	}
	function save_contact(contact_card){ //Accepts jQuery element of card
		var contact_data = objectifyForm(contact_card.find(':input').serializeArray());
		var contact_id = contact_card.data('id');
		var contact_doc;
		if(contact_id){
			contact_doc = client_doc.collection('contacts').doc(contact_id);
		} else {
			contact_doc = client_doc.collection('contacts').doc();
		}
		client_doc.get().then(function(doc){
			if(doc.exists){
				contact_doc.set(contact_data,{merge:true}).then(function(){
					console.log('success');
					build_contacts();
				});
			} else {
				batch.set(contact_doc, contact_data, {merge: true});
				alert('You must finish saving this new client to save your contact changes');
			}
		});
	}
</script>