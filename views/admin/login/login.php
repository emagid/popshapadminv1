<div id="firebaseui-auth-container" class="body bg-gray"></div>

<?php echo footer(); ?>
<script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.5/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-database.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.5.8/firebase-functions.js"></script>
<script src="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.js"></script>
<link type="text/css" rel="stylesheet" href="https://cdn.firebase.com/libs/firebaseui/3.4.1/firebaseui.css" />
<style>
	sidebar {
		display:none;
	}
</style>
<? echo footer();?>
<script>
	var token;
	var uiConfig = {
    	signInSuccessUrl: 'http://popshap.loc/dashboard',
    	signInOptions: [
    	  	// Leave the lines as is for the providers you want to offer your users.
			// firebase.auth.PhoneAuthProvider.PROVIDER_ID,  
			{
				provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
				requireDisplayName: true
			}
		],
		callbacks: {
			signInSuccessWithAuthResult: function(authResult, redirectUrl) {
				firebase.auth().currentUser.getIdToken(true).then(function(idToken) {
					token = idToken; 
					db.collection('users').doc(firebase.auth().currentUser.uid).set({
						user_role: (firebase.auth().currentUser.email.includes('@emagid.com') || firebase.auth().currentUser.email.includes('@popshap.com') )?'superadmin':'client',
						email: firebase.auth().currentUser.email
						// phone: firebase.auth().currentUser.phoneNumber
					},{ merge: true }).then(function(){
						verify_login(token);
					}).catch(function(error) {
						alert("Error writing document: "+ error);
    					console.error("Error writing document: ", error);
					});
				})
				// User successfully signed in.
				// Return type determines whether we continue the redirect automatically
				// or whether we leave that to developer to handle.
				return false;
			},
			uiShown: function() {
				// The widget is rendered.
				// Hide the loader.
				// document.getElementById('loader').style.display = 'none';
			}
		}
    	// tosUrl and privacyPolicyUrl accept either url string or a callback
    	// function.
    	// Terms of service url/callback.
    	// tosUrl: '<your-tos-url>',
    	// Privacy policy url/callback.
		// privacyPolicyUrl: '<your-privacy-policy-url>'
    };
</script>
<script type="text/javascript">
	function init() {
		$('sidebar').remove();
		var ui = new firebaseui.auth.AuthUI(firebase.auth()); 		
		firebase.auth().useDeviceLanguage();
		// window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('submit_btn', {
		// 	'size': 'invisible',
		// 	'callback': function(response) {
		// 		// reCAPTCHA solved, allow signInWithPhoneNumber.
		// 		onSignInSubmit();
		// 	}
		// });window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('submit_btn', {
		// 	'size': 'invisible',
		// 	'callback': function(response) {
		// 		// reCAPTCHA solved, allow signInWithPhoneNumber.
		// 		onSignInSubmit();
		// 	}
		// });
		ui.start('#firebaseui-auth-container', uiConfig);
	}

	function verify_login(token){
		var uid = firebase.auth().currentUser.uid
		$.post(
			'/admin/login/setToken',
			{uid: uid, uid_token:token}, 
			function(data){

				if(data.status){
					window.location.href = "/admin/dashboard";
				}
			});
	}

	// $("#admin_login").validate({
	// 	rules: {
	// 		username:{
	// 			required: true,
	// 		},
	// 		password:{
	// 			required: true,
	// 		}
	// 	},
	// 	messages:{
	// 		username:{ 
	// 			required:"Please enter your username",
	// 		},
	// 		password:{ 
	// 			required:"Please enter your password",
	// 		}
	// 	},
	// 	errorClass: "error"
	// }
	// )
</script>