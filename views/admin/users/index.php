<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> View Users</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th> ID </th>
                  <th> Role </th>
                  <th> Email </th>
                  <th> Phone </th>
                  <th> Notes </th>
                  <th> Delete </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    function init(){
      var users;
        db.collection('users')
          .get().then(function(querySnapshot) {
          querySnapshot.forEach(function(doc) {
            var row = $('<tr class="user">');
            row.data('id', doc.id);
            data = doc.data();
            row.append('<td>'+doc.id+'</td>');
            if(curr_user.user_role == 'superadmin'){
              var roles = $("<td><select class='form-control' name='role'>"+
                          "<option "+(data.user_role == 'client'?'selected':'') +" >client</option>"+
                          "<option "+(data.user_role == 'superadmin'?'selected':'') +" >superadmin</option>"+
                          "</select></td>");
              row.append(roles);
            } else {
              row.append('<td>'+data.user_role+'</td>');
            }
            if(doc.data().hasOwnProperty('email')){
              row.append('<td>'+data.email+'</td>');
            } else {
              row.append('<td></td>');
            }
            if(doc.data().hasOwnProperty('phone')){
              row.append('<td>'+data.phone+'</td>');
            } else {
              row.append('<td></td>');
            }
            if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
              row.append('<td><textarea class="form-control notes">'+(data.hasOwnProperty('notes')?data.notes:'')+'</textarea></td>');
            } else {
              row.append('<td>'+(data.hasOwnProperty('notes')?data.notes:'')+'</td>');
            }
            if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
              row.append('<td><a class="btn delete"> delete</a></td>');
            } else {
              row.append('<td></td>');
            }
            $('.items').append(row);
            // doc.data() is never undefined for query doc snapshots
              console.log(doc.id, " => ", doc.data());
          });
      });
      
      if(curr_user.user_role == 'superadmin'){
        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.user').data('id');
          var row = $(this).parents('tr.user');
          db.collection("users").doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('change','select[name=role]',function(){
          var id = $(this).parents('tr.user').data('id');
          db.collection('users').doc(id).set({
						user_role: this.value
						// phone: firebase.auth().currentUser.phoneNumber
					},{ merge: true }).then(function(){
					});
        });
        $(document).on('change','textarea.notes',function(){
          var id = $(this).parents('tr.user').data('id');
          db.collection('users').doc(id).set({
						notes: this.value
					},{ merge: true }).then(function(){
					});
        })
      }
    }
</script>