<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-plain">
          <div class="card-header card-header-primary">
            <h4 class="card-title mt-0"> <?=ucfirst($model->collection)?></h4>
            <p class="card-category"> Edit <select class="form-control" id="select_collection">
            <? foreach($model->menus as $collection) { ?>
              <?=$collection[count($collection)-1]?>
              <option value="<?=$collection?>"><?=ucfirst(str_replace('_',' ',$collection))?> options</option>
            <? } ?>
            </select> </p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-hover">
                <thead class="fields">
                  <th> ID </th>
                  <th> Name</th>
                  <!-- <th> Users </th> -->
                  <th colspan='2'> Edit </th>
                </thead>
                <tbody class="items">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo footer(); ?>
<script type="text/javascript">
    var option_collection;
    function loadOptions(){
      db.collection(option_collection)
        .get().then(function(querySnapshot) {
        $('.items').empty();
        querySnapshot.forEach(function(doc) {
          var row = $('<tr class="option">');
          row.data('id', doc.id);
          data = doc.data();
          row.append('<td>'+doc.id+'</td>');
          
          if(curr_user.user_role == 'superadmin' && data.user_role != 'superadmin'){
            row.append('<td><input class="form-control" type="text" placeholder="'+($('#select_collection option:selected').text())+' Name" name="size" value="'+(data.name?data.name:'')+'" /> </td>');
            row.append('<td><a class="btn save"> save</a></td>');
            row.append('<td><a class="btn delete"> delete</a></td>');
          } else {
            row.append('<td>'+(data.name?data.name:'')+'</td>');
            row.append('<td></td>');
            row.append('<td></td>');
          }
          $('.items').append(row);
          // doc.data() is never undefined for query doc snapshots
            console.log(doc.id, " => ", doc.data());
        });

        var new_row = $('<tr class="option">');

        new_row.append('<td> New '+($('#select_collection option:selected').text())+' </td>');
        new_row.append('<td><input class="form-control" placeholder="New '+($('#select_collection option:selected').text())+' Name" name="name" /></td>');
        new_row.append('<td colspan="2"><a class="btn add"> Add</a></td>');

        if(curr_user.user_role == 'superadmin'){
          $('.items').append(new_row);
        }

      });
    } 
    function init(){
      
      if(curr_user.user_role == 'superadmin'){
        $(document).on('click','a.btn.add',function(){
          var data = {};
          var row = $(this).parents('tr.option');
          var id;
          row.find(':input').serializeArray().forEach(function(elem) {
            if(elem.name != 'id'){
              data[elem.name] = (elem.value == ''?null:elem.value);
            } else {
              id = elem.value;
            }
          });
          db.collection(option_collection)
           .add(data)
           .then(function(){
             loadOptions();
           }); 
        });

        $('#select_collection').change(function(){
          option_collection = this.value;
          loadOptions();
        }).change();
        $(document).on('click','a.btn.delete',function(){
          var id = $(this).parents('tr.option').data('id');
          var row = $(this).parents('tr.option');
          db.collection(option_collection).doc(id).delete().then(function() {
              row.remove();
              console.log("Document successfully deleted!");
          }).catch(function(error) {
              console.error("Error removing document: ", error);
          }); 
        });
        $(document).on('click','a.btn.save',function(){
          var id = $(this).parents('tr.option').data('id');
          var data = {};
          var row = $(this).parents('tr.option');
          row.find(':input').serializeArray().forEach(function(elem) {
            if(elem.name != 'id'){
              data[elem.name] = (elem.value == ''?null:elem.value);
              if(elem.name == 'contact_email' && elem.value.includes(',') ){
                data[elem.name] = elem.value.split(',');
              }
            }
          });
          db.collection(option_collection).doc(id).update(data)
          .then(function() {
              console.log("Document successfully updated!");
          })
          .catch(function(error) {
              // The document probably doesn't exist.
              console.error("Error updating document: ", error);
          });
        });
        // $(document).on('change','textarea.notes',function(){
        //   var id = $(this).parents('tr.user').data('id');
        //   db.collection('users').doc(id).set({
				// 		notes: this.value
				// 	},{ merge: true }).then(function(){
				// 	});
        // });
      }
    }
</script>