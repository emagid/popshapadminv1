<?php

use Emagid\Core\Membership;
use Firebase\Auth\Token\Verifier;

/**
 * Base class to control login
 */

class loginController extends \Emagid\Mvc\Controller {
	
	protected $_credentials;

	function __construct(){
		
		parent::__construct();
		ob_start();
		require(FIREBASE_CREDENTIALS);
		$this->_credentials = ob_get_clean();
		$this->_credentials = json_decode($this->_credentials);

	}

	public function login(){
		if(isset($_SESSION['uid'])){
			redirect('/admin/dashboard/index');
		}

		$model = new \stdClass();
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}
	
	public function login_post()
	{	
		if (\Model\Admin::login($_POST['username'],$_POST['password'])){
			redirect(ADMIN_URL . 'dashboard/index');
		} 
		/* 
		$admin = new \Model\Admin();
		$admin->first_name = 'Master';
		$admin->last_name = 'User';
		$admin->email = 'master@user.com';
		$admin->username = 'emagid';
		$admin->password = 'test5343';
		$admin->permissions = 'Administrators';

		$hash = Membership::hash($admin->password);
		$admin->password = $hash['password'];
		$admin->hash = $hash['salt'];

		if ($admin->save()){
			$adminRoles = \Model\Admin_Roles::getList(['where' => 'active = 1 and role_id = 1 and admin_id = '.$admin->id]);
			if (count($adminRoles) <= 0){
				$adminRole = new \Model\Admin_Roles();
				$adminRole->role_id = 1;
				$adminRole->admin_id = $admin->id;
				$adminRole->save();
			}
		};
		echo 'BITCH';*/
		
		$model = new \stdClass();
		$model->errors = ['Invalid username or password'];
		$model->page_title = 'Login';
		$model->logged_admin = null;
		$model->admin_sections = [];
		$this->loadView($model);
	}

	public function logout(){
		Membership::destroyAuthenticationSession();
		unset($_SESSION['uid']);
		redirect(ADMIN_URL . 'login');
	}

	public function setToken_post(){
		$uid = $_POST['uid'];
		$uidToken = $_POST['uid_token'];

		$status = $this->verify_token($uidToken,$uid);

		if($status === true){
			$_SESSION['uid'] = $uid;
		}

		$this->toJson(['status'=> $status]);
	}

	function verify_token($token,$uid) {
		$verifier = new Verifier($this->_credentials->project_id);
		return true;
		try {
			$verifiedIdToken = $verifier->verifyIdToken($token);

			return true;
		} catch (\Firebase\Auth\Token\Exception\ExpiredToken $e) {
			return $e->getMessage();
		} catch (\Firebase\Auth\Token\Exception\IssuedInTheFuture $e) {
			return $e->getMessage();
		} catch (\Firebase\Auth\Token\Exception\InvalidToken $e) {
			return $e->getMessage();
		}
	}

	public function toJson($array)
    {
        header('Content-Type: application/json');
        echo json_encode($array);
        exit();
    }

}