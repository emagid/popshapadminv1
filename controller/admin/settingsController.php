<?php

class settingsController extends adminController {
	
	function __construct(){
		parent::__construct("Config");
		
		$this->_viewData->menus = [
			'device_types',
			'project_type',
			'locations',
			'project_status'	
		];
		$this->_viewData->collection = 'settings';
	}

	public function index(Array $params = []){
		parent::index($params);
	}

	public function update_post() {
    	$obj = \Model\Config::getItem($_POST['id']); 
    	
    	$obj->value = $_POST['value'];

    	if ($obj->save()){
            $this->update_relationships($obj);
            $this->afterObjSave($obj);
            $content = str_replace("\Model\\", "", $this->_model);
            $content = str_replace('_', ' ', $content);
            $n = new \Notification\MessageHandler(ucwords($content).' saved.');
           	$_SESSION["notification"] = serialize($n);
    	} else {
    		$n = new \Notification\ErrorHandler($obj->errors);
           	$_SESSION["notification"] = serialize($n);
           	redirect(ADMIN_URL.$this->_content.'/update/'.$obj->id);
    	}

    	if (isset($_POST['redirectTo'])){
    		redirect($_POST['redirectTo']);
    	} else {
    		redirect(ADMIN_URL.$this->_content);
    	}
    }

	function delete0(Array $arr = []){}
  	
}